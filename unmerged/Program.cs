﻿using Aspose.Cells;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Text;

namespace unmerged
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            

            while (true)
            {

                Console.WriteLine("Copy & Paste file to unmerge");
                string @filename = Console.ReadLine();

                filename = filename.Replace("\"", "");
                Workbook wb = new Workbook(filename);



                foreach (Worksheet ws in wb.Worksheets)
                {
                    if (ws.Name.StartsWith("~"))
                    {
                        ws.IsVisible = false;
                    }


                    for (int row = 0; row < ws.Cells.MaxDataRow + 1; row++)
                    {
                        for (int col = 0; col < ws.Cells.MaxDataColumn + 1; col++)
                        {
                            if (ws.Cells[row, col].IsFormula)
                            {
                                Console.WriteLine($"Tab: {ws.Name} Contains Formula: {row}, {col}");
                                if(ws.Cells[row, col].Value != null)
                                {
                                   // ws.Cells[row, col].Value = ws.Cells[row, col].Value.ToString();
                                }
                                

                            }
                            /*
                            if( row > 0)
                            {
                                string color = ColorTranslator.ToHtml( Color.FromArgb( ws.Cells[row,0].GetStyle().BackgroundColor.ToArgb()));
                                color = color.ToUpper();
                                if (color == "#93C47D")
                                {
                                    Console.WriteLine($"{ws.Name}::ROW {row}::Color {color}");
                                    ws.Cells[row, 11].Value = "*";
                                }
                                if (color == "#FFF2CC")
                                {
                                    Console.WriteLine($"{ws.Name}::ROW {row}::Color {color}");
                                    ws.Cells[row, 11].Value = "*";
                                }
                                if (color == "#D9EAD3")
                                {
                                    Console.WriteLine($"{ws.Name}::ROW {row}::Color {color}");
                                    ws.Cells[row, 11].Value = "*";
                                }
                                if (color == "#B6D7A8")
                                {
                                    Console.WriteLine($"{ws.Name}::ROW {row}::Color {color}");
                                    ws.Cells[row, 11].Value = "*";
                                }
                                if (color == "#FFFF00")
                                {
                                    Console.WriteLine($"{ws.Name}::ROW {row}::Color {color}");
                                    ws.Cells[row, 11].Value = "*";
                                }
                            }
                            */
                        }
                    }


                    ArrayList mergedcells = ws.Cells.MergedCells;
                    foreach (CellArea mcell in mergedcells)
                    {

                        Console.WriteLine($"Tab: {ws.Name} - Cell Start: {mcell.StartRow}::{mcell.StartColumn} Cell End: {mcell.EndRow - mcell.StartRow + 1},{mcell.EndColumn - mcell.StartColumn + 1}");
                        ws.Cells.UnMerge(mcell.StartRow, mcell.StartColumn, mcell.EndRow - mcell.StartRow + 1, mcell.EndColumn - mcell.StartColumn + 1);
                    }



                }

                wb.Save(filename.Replace(".xlsx", "_updated.xlsx"));
                Console.WriteLine("done");
                //remove eval
                RemoveEval(filename.Replace(".xlsx", "_updated.xlsx"));
            }


            
        }

        static void RemoveEval(string file)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            ExcelPackage wb = new ExcelPackage(new FileInfo(file));
            for (int i = 0; i < wb.Workbook.Worksheets.Count; i++)
            {
                
                if (wb.Workbook.Worksheets[i].Name.Contains("Evaluation Warning"))
                {
                    wb.Workbook.Worksheets.Delete(i);
                }


            }

            wb.Save();
            
        }

    }
}
